public class EncAndDec {
    public static void main(String[] args) {
        String op = args[0];
        int key = Integer.parseInt(args[2]);
        String encrypt = new String("enc");
        String decrypt = new String("dec");
        Caeser Transform = new Caeser();
        if (op.equals(encrypt)) {
            String plaintext = args[1];
            try {
                String ciphertext = Transform.encrypt(plaintext, key);
                System.out.print(getS(ciphertext));
            } catch (E e) {
                System.out.println(e.warnMess());
            }
        } else if (op.equals(decrypt)) {
             String ciphertext = args[1];
            try {
                String plaintext = Transform.decrypt(ciphertext, key);
                System.out.print(getS(plaintext));
            } catch (E e) {
                System.out.println(e.warnMess());
            }
        }
    }
    private static String getS(String text) {
        return "plaintext:" + text;
    }
}