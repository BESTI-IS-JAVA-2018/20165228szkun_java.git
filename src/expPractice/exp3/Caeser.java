public class Caeser {
    String encrypt(String sourceString, int key) throws E {
        char[] c = sourceString.toCharArray();
        int m = c.length;
        for (int i = 0; i < m; i++) {
            if (c[i] > 122 || c[i] < 65 || (c[i] > 90 && c[i] < 97)) {
                throw new E(sourceString);
            }
            c[i]= (char)((c[i] - 'a' + key) % 26 + 'a');          //加密
        }
        return new String(c);                                       //返回密文
    }

    String decrypt(String sourceString, int key) throws E {   //解密算法
        char[] c = sourceString.toCharArray();
        int m = c.length;
        for (int i = 0; i < m; i++) {
            if (c[i] > 122 || c[i] < 65 || (c[i] > 90 && c[i] < 97)) {
                throw new E(sourceString);
            }
            int n = c[i] - 'a' - key + 26;
            c[i] =(char)( n % 26 + 'a');          //解密
        }
        return new String(c);                          //返回明文
    }
}