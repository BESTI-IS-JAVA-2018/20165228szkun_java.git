import junit.framework.TestCase;
import org.testng.annotations.Test;
public class ComplexTest extends TestCase {
    Complex complex1=new Complex(5.0,1.0);
    Complex complex2=new Complex(-1.0,3.0);
    @Test
    public void getRealPart(){
        assertEquals("5.0",complex1.getRealPart());
        assertEquals("-1.0",complex2.getRealPart());
    }
    @Test
    public void getImagePart(){
        assertEquals("1.0",complex1.getImagePart());
        assertEquals("3.0",complex2.getImagePart());
    }
    @Test
    public void testComplexAdd() {
        assertEquals("4.0+4.0i",complex1.ComplexAdd(complex2).toString());
    }
    @Test
    public void testComplexSub() {
        assertEquals("6.0-2.0i",complex1.ComplexSub(complex2).toString());
    }
    @Test
    public void testComplexMulti() {
        assertEquals("-8.0+14.0i",complex1.ComplexMulti(complex2).toString());
    }
    @Test
    public void testComplexDiv() {
        assertEquals("-0.2-1.6i", complex1.ComplexDiv(complex2).toString());
    }
    @Test
    public void testtoString() {
        assertEquals("5.0+1.0i",complex1.toString());
        assertEquals("-1.0+3.0i",complex2.toString());
    }
    @Test
    public void testequals() {
        assertEquals(false,complex1.equals(complex2));
        Complex c = new Complex(complex1.getRealPart(),complex1.getImagePart());
        assertEquals(true,complex1.equals(c));
    }
}