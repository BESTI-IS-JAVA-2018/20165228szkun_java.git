/*（1）属性：复数包含实部和虚部两个部分，
double RealPart;复数的实部
double ImagePart;复数的虚部
getRealPart():返回复数的实部
getImagePart();返回复数的虚部
setRealPart():设置复数的实部
setImagePart();设置复数的虚部
输出形式：a+bi
（2）方法：
①定义构造函数
public Complex()
public Complex(double R,double I)
②定义公有方法:加减乘除
Complex ComplexAdd(Complex a):实现复数加法
Complex ComplexSub(Complex a):实现复数减法
Complex ComplexMulti(Complex a):实现复数乘法
Complex ComplexDiv(Complex a):实现复数除法
③Override Object
public String toString():将计算结果转化为字符串形式并输出
*/
public class Complex {
    private double RealPart;//复数的实部
    private double ImagePart;//复数的虚部
    public Complex() {}
    public Complex(double R,double I){
        setRealPart(R);
        setImagePart(I);
    }
    public void setRealPart(double R){
        RealPart=R;//设置复数的实部
    }
    public void setImagePart(double I){
        ImagePart=I;//设置复数的虚部
    }
    public double getRealPart(){
        return RealPart;//返回复数的实部
    }
    public double getImagePart(){
        return ImagePart;//返回复数的虚部
    }
    Complex ComplexAdd(Complex a) {
        Complex New =new Complex();
        New.RealPart=this.RealPart+a.RealPart;
        New.ImagePart=this.ImagePart+a.ImagePart;
        return New;
    }
    Complex ComplexSub(Complex a){
        Complex New =new Complex();
        New.RealPart=this.RealPart-a.RealPart;
        New.ImagePart=this.ImagePart-a.ImagePart;
        return New;
    }
    Complex ComplexMulti(Complex a){
        Complex New =new Complex();
        New.RealPart=this.RealPart*a.RealPart-this.ImagePart*a.ImagePart;
        New.ImagePart=this.ImagePart*a.RealPart+this.RealPart*a.ImagePart;
        return New;
    }
    Complex ComplexDiv(Complex a){
        double sum=a.ImagePart*a.ImagePart+a.RealPart*a.RealPart;
        Complex b = new Complex(a.getRealPart()/sum, -a.getImagePart()/sum);
        return ComplexMulti(b);
    }
    @Override
    public String toString() {
        if(ImagePart>0.0)
            return RealPart+"+"+ImagePart+"i";
        else if(ImagePart==0.0)
            return RealPart+" ";
        else
            return RealPart+""+ImagePart+"i";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Complex complex = (Complex) o;
        return Double.compare(complex.getRealPart(), getRealPart()) == 0 && Double.compare(complex.getImagePart(), getImagePart()) == 0;
    }
}