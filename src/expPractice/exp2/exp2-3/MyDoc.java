abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Float extends  Data {
    float value;
    Float() {
        value=52.28F;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new FloatFactory());
        d.DisplayData();
    }
}