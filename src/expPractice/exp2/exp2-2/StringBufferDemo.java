public class StringBufferDemo{
   public static void main(String [] args){
               StringBuffer buffer = new StringBuffer(20);
               buffer.append('S');  //调用方法append，缓存char型
               buffer.append("tringBuffer");  //调用方法append，缓存String型
               System.out.println(buffer.charAt(1));  //返回指定索引位置1的char值
               System.out.println(buffer.capacity());  //返回字符串所占容器的总大小
               System.out.println(buffer.indexOf("tring12345"));  //从第1个位置查找字符串"tring12345"第一次出现的位置
               System.out.println("buffer = " + buffer.toString());  //返回字符串本身
               System.out.println(buffer.length());//返回字符串的实际长度
          }
}