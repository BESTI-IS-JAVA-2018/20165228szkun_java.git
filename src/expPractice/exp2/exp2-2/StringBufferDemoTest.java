import junit.framework.TestCase;
import org.testng.annotations.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer one= new StringBuffer("String");
    StringBuffer two = new StringBuffer("StringBuffer");
    StringBuffer three = new StringBuffer("StringBufferStringBuFFER");
    @Test
    public void testcharAt() throws Exception{
        assertEquals('S',one.charAt(0));
        assertEquals('g',two.charAt(5));
        assertEquals('i',three.charAt(15));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(22,one.capacity());
        assertEquals(28,two.capacity());
        assertEquals(40,three.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(6,one.length());
        assertEquals(12,two.length());
        assertEquals(24,three.length());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(3,one.indexOf("ing"));
        assertEquals(1,two.indexOf("tr"));
        assertEquals(20,three.indexOf("FFER"));
    }
}