import java.util.Scanner;
import java.math.BigDecimal;
public class Calculator {
    public static void main(String[] args) {
        String name1="+";
        String name2="-";
        String name3="*";
        String name4="/";
        Scanner scanner=new Scanner(System.in);
        System.out.printf("选择四则运算:");
        String str=new String(scanner.nextLine());
        if(name1.equals(str)){
            System.out.printf("请输入需要运算的两个数字，以空格分割:");
            BigDecimal a=new BigDecimal(scanner.nextFloat());
            BigDecimal b=new BigDecimal(scanner.nextFloat());
            System.out.printf(a+"+"+b +"结果是%f\n",a.add(b));
        }
        else if(name2.equals(str)){
            System.out.printf("请输入需要运算的两个数字，以空格分割:");
            BigDecimal a=new BigDecimal(scanner.nextFloat());
            BigDecimal b=new BigDecimal(scanner.nextFloat());
            System.out.printf(a+"-"+b +"结果是%f\n",a.subtract(b));
        }
        else if(name3.equals(str)){
            System.out.printf("请输入需要运算的两个数字，以空格分割:");
            BigDecimal a=new BigDecimal(scanner.nextFloat());
            BigDecimal b=new BigDecimal(scanner.nextFloat());
            System.out.printf(a+"*"+b +"结果是%f\n",a.multiply(b));
        }
        else if(name4.equals(str)){
            System.out.printf("请输入需要运算的两个数字，以空格分割:");
            BigDecimal a=new BigDecimal(scanner.nextFloat());
            BigDecimal b=new BigDecimal(scanner.nextFloat());
            System.out.printf(a+"/"+b +"结果是%f\n",a.divide(b));
        }
        else System.out.printf("输入错误");
    }
}