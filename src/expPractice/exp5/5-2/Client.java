import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {
    public static final String IP_ADDR = "127.0.0.1";
    public static final int PORT = 12345;
    public static void main(String[] args) {
        System.out.println("客户端启动中...");
        while (true) {
            Socket socket = null;
            try {
                socket = new Socket(IP_ADDR, PORT);
                DataInputStream input = new DataInputStream(socket.getInputStream());
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                System.out.print("请输入需要计算的式子: \t");
                String str = new BufferedReader(new InputStreamReader(System.in)).readLine();
                MyBC turner = new MyBC();
                String str1 = turner.turn(str);
                int length=0,i=0;
                while(str1.charAt(i)!='\0'){
                    length++;
                    i++;
                }
                String str2 = str1.substring(1,length-1);
                out.writeUTF(str2);
                String ret = input.readUTF();
                System.out.println("服务器端返回数值: " + ret);
                out.close();
                input.close();
            } catch (Exception e) {
                System.out.println("客户端出现异常:" + e.getMessage());
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        socket = null;
                        System.out.println("客户端 finally 异常:" + e.getMessage());
                    }
                }
            }
        }
    }
}