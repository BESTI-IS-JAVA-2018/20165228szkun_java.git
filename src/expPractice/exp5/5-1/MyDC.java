import java.util.StringTokenizer;
import java.util.Stack;

public class MyDC
{
    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MULTIPLY = '*';
    private final char DIVIDE = '/';
    private Stack<Integer> stack;

    public MyDC() {
        stack = new Stack<Integer>();
    }

    public int evaluate (String expr)
    {
        int op1, op2, result = 0;
        String token;
        String str=" ";
        StringBuilder string = new StringBuilder(str);
        StringTokenizer tokenizer = new StringTokenizer (expr);

        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();
            if (isOperator(token))
            {
                op2 =stack.pop();
                op1 =stack.pop();
                result = evalSingleOp(token.charAt(0),op1,op2);
                stack.push(result);
            }
            else
                stack.push(Integer.parseInt(token));
        }

        return result;
    }

    private boolean isOperator (String token)
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }

    public int evalSingleOp (char operation, int op1, int op2)
    {
        int result = 0;

        switch (operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }

        return result;
    }
}