import junit.framework.Test;
import junit.framework.TestSuite; 
import junit.framework.TestCase; 

/** 
* MyBC Tester. 
* 
* @author <Authors name> 
* @since <pre>05/27/2018</pre> 
* @version 1.0 
*/
import java.util.Scanner;

public class MyBCTest  {
    public static void main (String[] args) {

        String expression1=null,expression2=null, again=null;

        int result,i=0,length=0;

        try
        {
            Scanner in = new Scanner(System.in);

            do
            {
                MyDC evaluator = new MyDC();
                MyBC turner = new MyBC();
                System.out.println ("请输入一个后缀表达式: ");
                expression1 = in.nextLine();
                expression2 = turner.turn(expression1);
                while(expression2.charAt(i)!='\0'){
                    length++;
                    i++;
                }
                expression2 = expression2.substring(1,length-1);
                result = evaluator.evaluate (expression2);
                System.out.println();
                System.out.println ("表达式的结果为: " + result);

                System.out.print ("是否输入一个新的表达式[Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}