import java.util.StringTokenizer;
import java.util.Stack;

public class MyBC {
    private Stack<Character> stack1;

    public char Precede(char a, char b) {
        if (a == '#')
            if (b != '#')
                return '<';
            else
                return '=';
        if (a == ')')
            return '>';
        if (a == '(')
            if (b != ')')
                return '<';
            else
                return '=';
        if (a == '/' || a == '*')
            if (b != '(')
                return '>';
            else
                return '<';
        if (a == '-' || a == '+')
            if (b != '*' && b != '/' && b != '(')
                return '>';
            else
                return '<';
        return '>';
    }

    public MyBC() {
        stack1 = new Stack<Character>();
        stack1.push('#');
    }

    public String turn(String expr) {
        int result = 0;
        String token;
        char topelem, optr;
        char[] exper1 = new char[100];
        int i = 0;

        StringTokenizer tokenizer = new StringTokenizer(expr);

        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();

            if (isOperator(token)) {

                topelem = stack1.peek();

                optr = token.charAt(0);

                if (Precede(topelem, optr) == '<') {

                    stack1.push(optr);

                } else if (Precede(topelem, optr) == '=') {
                    optr = stack1.pop();
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                } else if (Precede(topelem, optr) == '>') {
                    optr = stack1.pop();
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                }
            } else if (token.equals("(")) {
                optr = token.charAt(0);
                stack1.push(optr);
            } else if (token.equals(")")) {
                optr = stack1.pop();
                while (optr != '(') {
                    exper1[i++] = optr;
                    exper1[i++] = ' ';
                    optr = stack1.pop();
                }
            } else {
                optr = token.charAt(0);
                //System.out.println(optr);
                exper1[i++] = optr;
                exper1[i++] = ' ';

            }
        }
        while (!stack1.isEmpty()) {
            optr = stack1.pop();
            if (optr != '#') {
                exper1[i++] = optr;
                exper1[i++] = ' ';
            }
        }

        return ToString(exper1);
    }

    //@Override
    private boolean isOperator(String token) {

        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    public static String ToString(char[] exper1) {
        int length = exper1.length;
        String str = " ";
        for (int i = 0; i < length; i++) {
            str = str + exper1[i];
        }
        return str;
    }
}