public class Score {
	private int No;
	private int score;
	public ScoreClass() {
		No = 1000;
		score = 0;
	}
	public ScoreClass(int n,int s) {
		No = n;
		score = s;
	}
	public void setInfo(int n, int s) {
		No = n;
		score = s;
	}
	public int getNo() {
		return No;
	}
	public int getScore() {
		return score;
	}
        @Override
	public String toString() {
		return No + "\t" + score;
	}
        @Override
	public boolean equals(Object obj) {
	if (this == obj)
			return true;
	}
	if(obj == null) {
		return false;
	}
	if (getClass() !=obj.getClass()) {
		return false;
	}
Scoreclass other = (ScoreClass) obj;
	if (other.No == No && other.score == score) {
		return true;
	} else{
		return false;
	}
}
}
