import java.util.Scanner;  
 public class JavaDay001_5 {  
    public static void main(String [] args)  
    {  
        System.out.println("请输入学生学号：");  
        Scanner scanner = new Scanner(System.in);  
        int nStudent = scanner.nextInt();  
        int nStudentYear = nStudent / 10000;  
        int nStudentTime = (nStudent / 100) % nStudentYear;  
        int nStudentId = nStudent % (nStudent / 100);  
        System.out.println("学生学号:" + nStudent);  
        System.out.println("学生入学年份:" + nStudentYear + "年");  
        System.out.println("学生期数:" + nStudentTime + "期");  
        System.out.println("学生序号:" + nStudentId + "号");  
    }  
}  