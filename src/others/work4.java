public class work4 {
    public static void main(String args[]) {
        int [] tmp = new int [args.length];
        for(int i=0; i<args.length; i++) {
            tmp[i] = Integer.parseInt(args[i]);
        }
        double sum = 0.00;
        for (int i=1;i<=tmp[0];i++) {
            sum += fact(i);
        }
        System.out.println(sum);

    }
    public static double fact(double n) {
        if (n == 1)
            return 1;
        else
            return 1/n * fact(n-1);
    }
}