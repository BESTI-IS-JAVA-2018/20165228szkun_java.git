import java.util.Scanner;
import java.util.Random;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Stack;

public class Arithmetic {
    private static int level;
    private static int number;
    static Random generator = new Random();
    static int x = 0;
    static int answer;
    static int key;
    public static void main(String args[])throws IOException{
        Scanner scan = new Scanner(System.in);
        System.out.println("请确认等级（1-4级）： ");
        level = scan.nextInt();
        System.out.println("题目数量：");
        number = scan.nextInt();
        if(level == 1) {
            int right = 0;
            String[] operation = {"+", "-"};
            double rate;
            while (x != number) {
                int a, b, op;
                a = generator.nextInt(100);
                b = generator.nextInt(100);
                op = generator.nextInt(2);
                System.out.printf("%d %s %d =", a, operation[op], b);
                answer = scan.nextInt();
                if (op == 0)
                    key = a + b;
                else if (op == 1)
                    key = a - b;
                System.out.println("你的答案是：" + answer);
                if (answer == key) {
                    System.out.println("正确！");
                    right++;
                } else
                    System.out.println("错误！正确答案是" + key);
                x++;
            }
            rate = (double) right / number;
            System.out.println("检测结束，正确率是：" + rate);
        }
        else if(level == 2){
            int right = 0;
            String[] operation = {"*", "/"};
            double rate;
            while (x != number){
                int a, b, op;
                a = generator.nextInt(51);
                b = generator.nextInt(51);
                op = generator.nextInt(2);
                System.out.printf("%d %s %d =", a, operation[op],b);
                answer = scan.nextInt();
                if (op == 0)
                    key = a * b;
                else if (op == 1)
                    key = a / b;
                System.out.println("你的答案是：" + answer);
                if (answer == key) {
                    System.out.println("正确！");
                    right++;
                }else
                    System.out.println("错误！");
                x++;
            }
            rate = (double)right / number;
            System.out.println("检测结束，正确率是 " + rate);
        }
        else if(level == 3){
            int right = 0;
            double rate;
            String[] operation = {"+","-","*", "/"};
            RationalNumber key;
            key = null;

            while (x != number){
                int a, b, c, d;
                int op;
                a = generator.nextInt(10) + 1;
                b = generator.nextInt(10) + 1;
                c = generator.nextInt(10) + 1;
                d = generator.nextInt(10) + 1;
                op = generator.nextInt(4);
                RationalNumber r1 = new RationalNumber(a, b);
                RationalNumber r2 = new RationalNumber(c, d);

                System.out.printf( r1 + operation[op] + r2 + "=");

                InputStreamReader i=new InputStreamReader(System.in);
                BufferedReader j=new BufferedReader(i);

                String answer = j.readLine();

                if (op == 0)
                    key = r1.add(r2);
                else if (op == 1)
                    key = r1.subtract(r2);
                else if (op == 2)
                    key = r1.multiply(r2);
                else if (op == 3)
                    key = r1.divide(r2);
                String key1;
                key1 = key.toString();


                System.out.println("你的答案是： " + answer);
                
                if (key1.equals(answer)) {
                    System.out.println("正确！");
                    right++;
                }
                else
                    System.out.println("错误!正确答案是" + key1);
                x++;
            }
            rate =(double) right / number;
            System.out.println("检测结束，正确率是 " + rate);
        }
        else if(level == 4){
            Evaluator q = new Evaluator();
            InfixToSuffix w = new InfixToSuffix();
            int a, b, c, d, op1, op2, op3;
            int right = 0;
            double rate;
            String equation, suffix;
            String[] operation = {"+","-","*", "/"};

            while (x != number) {
               a = generator.nextInt(10);
               b = generator.nextInt(10);
               c = generator.nextInt(10);
               d = generator.nextInt(10);
               op1 = generator.nextInt(4) + 1;
               op2 = generator.nextInt(4) + 1;
               op3 = generator.nextInt(4) + 1;

                equation =  a + " " + operation[op1] + " " + b + " "  + operation[op2] + " " + c + " " + operation[op3] + " " + d;

               suffix = w.InfixToSuffix(equation);
               answer = q.evaluate(suffix);

               System.out.print(equation + " =");
               int answers = scan.nextInt();

               if (answers == answer) {
                   System.out.println("正确！");
                   right++;
               } else
                   System.out.println("错误！");
               x++;
           }
            rate = (double)right / number;
            System.out.println("检测结束，正确率是 " + rate);
        }
    }
}

class RationalNumber {
    private int numerator, denominator;
    public RationalNumber(int i, int j) {
        if (j == 0)
            j = 1;
        if (j < 0) {
            i = i * -1;
            j = j * -1;
        }
        numerator = i;
        denominator = j;
        reduce();
    }
    public int getNumerator() {
        return numerator;
    }
    public int getDenominator() {
        return denominator;
    }
    public RationalNumber reciprocal() {
        return new RationalNumber(denominator, numerator);
    }
    public RationalNumber add(RationalNumber op2) {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;
        return new RationalNumber(sum, commonDenominator);
    }
    public RationalNumber subtract(RationalNumber op2) {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 - numerator2;
        return new RationalNumber(difference, commonDenominator);
    }
    public RationalNumber multiply (RationalNumber op2) {
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();
        return new RationalNumber (numer, denom);
    }
    public RationalNumber divide (RationalNumber op2) {
        return multiply (op2.reciprocal());
    }
    public boolean isLike (RationalNumber op2) {
        return ( numerator == op2.getNumerator() && denominator == op2.getDenominator());
    }
    public String toString () {
        String result;
        if (numerator == 0)
            result = "0";
        else
        if (denominator == 1)
            result = numerator + "";
        else
            result = numerator + "/" + denominator;
        return result;
    }
    private void reduce () {
        if (numerator != 0) {
            int common = gcd (Math.abs(numerator), denominator);
            numerator = numerator / common;
            denominator = denominator / common;
        }
    }
    private int gcd (int num1, int num2) {
        while (num1 != num2)
            if (num1 > num2) num1 = num1 - num2; else num2 = num2 - num1;
        return num1;
    }
}
class Evaluator {
    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MULTIPLY = '*';
    private final char DIVIDE = '/';
    private Stack<Integer> stack;
    public Evaluator() {
        stack = new Stack<Integer>();
    }
    public int evaluate (String expr)
    {
        int op1, op2, result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);
        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();
            if (isOperator(token))
            {
                int b = stack.pop();
                int a = stack.pop();
                result = evalSingleOp(token.charAt(0),a,b);
                stack.push(result);
                result = stack.push(result);
            }
            else
            {
                stack.push(Integer.parseInt(token));
            }
        }
        return result;
    }
    private boolean isOperator (String token)
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }

    private int evalSingleOp (char operation, int op1, int op2)
    {
        int result = 0;

        switch (operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }

        return result;
    }
}
class InfixToSuffix {
    public String InfixToSuffix(String infix) {
        Stack<String> stack = new Stack<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix);
        String suffix = "";
        String temp;
        while (tokenizer.hasMoreTokens()) {
            String c = tokenizer.nextToken();
            switch (c) {
                case " ":
                    break;
                case "(":
                    stack.push(c);
                    break;
                case "+":
                case "-":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(") {
                            stack.push("(");
                            break;
                        }
                        suffix += " " + temp;
                    }
                    stack.push(c);
                    suffix += " ";
                    break;
                case "*":
                case "/":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(" || temp == "+" || temp == "-") {
                            stack.push(temp);
                            break;
                        } else {
                            suffix += " " + temp;
                        }
                    }
                    stack.push(c);
                    suffix += " ";
                    break;
                case ")":
                    while (stack.size() != 0) {
                        temp = stack.pop();
                        if (temp == "(")
                            break;
                        else
                            suffix += " " + temp;
                    }
                    break;
                default:
                    suffix += c;
            }
        }
        while (stack.size() != 0) {
            suffix += " " + stack.pop();
        }
        return suffix;
    }
}
