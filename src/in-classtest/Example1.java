import java.sql.*;
public class Example1 {
    public static void main(String [] args) {
        Connection con=null;
        Statement sql;
        ResultSet rs;
        try{  Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){}
        String uri = "jdbc:mysql://localhost:3306/world?useSSL=true";
        String user ="root";
        String password ="";
        try{
            con = DriverManager.getConnection(uri,user,password);
        }
        catch(SQLException e){ }
        try {
            sql=con.createStatement();
            rs=sql.executeQuery("SELECT * FROM city");
            while(rs.next()) {
                int ID=rs.getInt(1);
                String Name=rs.getString(2);
                String CountryCode =rs.getString(3);
                String District =rs.getString(4);
                int Population=rs.getInt(5);
                if(Population>1016522) {
                    System.out.printf("%d\t", ID);
                    System.out.printf("%s\t", Name);
                    System.out.printf("%s\t", CountryCode);
                    System.out.printf("%s\n", District);
                    System.out.printf("%d\n", Population);
                }
            }
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}
