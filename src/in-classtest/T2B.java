package szk;
import java.io.*;
import java.util.*;
public class T2B {
    public static void main(String args[]) {
        File fRead = new File("20165228.txt");
        File fWrite = new File("20165228.bin");
        try{  Writer out = new FileWriter(fWrite);
            BufferedWriter bufferWrite = new BufferedWriter(out);
            Reader in = new FileReader(fRead);
            BufferedReader bufferRead =new BufferedReader(in);
            String str = null;
            for(int i=0;i<10;i++) {
                str=bufferRead.readLine();
                bufferWrite.write(str);
                bufferWrite.newLine();
            }
            bufferWrite.close();
            out.close();
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
    }
}