public class szkun20165228 {
    public static void main(String args[]) {
        int [] tmp = new int [args.length];
        for(int i=0; i<args.length; i++) {
            tmp[i] = Integer.parseInt(args[i]);
        }
        int sum = 0;
        for (int i=1;i<=tmp[0];i++) {
            sum += fact(i);
        }
        System.out.println(sum);
    }

    public static int fact(int n) {
        if (n == 0)
            return 1;
        else
            return n * fact(n-1);
    }
}