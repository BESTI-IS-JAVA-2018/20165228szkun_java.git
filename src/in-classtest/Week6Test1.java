public class Week6Test1 {
    public static void main(String args[]) {
        int c = 0;
        if(args.length < 1) {
            System.out.println("Error!Please input the right statistics");
            System.exit(0);
        }
        int tmp[] = new int[args.length];
        for(int i=0;i<args.length;i++) {
            tmp[i] = Integer.parseInt(args[i]);
        }
        if(tmp[0]<0 || tmp[1]<0 || tmp[0]<tmp[1]) {
            System.out.println("Your numbers are incorrect!Please input again");
            System.exit(0);
        }
        else {
            c  = Calculate(tmp[0],tmp[1]);
        }
        System.out.println(c);
    }
    public static int Calculate(int n,int m) {
        if(n == m || m == 0)
            return 1;
        else
            return Calculate(n-1,m-1)+Calculate(n-1,m);
    }
}