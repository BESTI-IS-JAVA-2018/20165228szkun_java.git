package week12;
import java.io.*;
import java.util.*;
public class B2T {
    public static void main(String args[]) {
        File fRead = new File("20165228.bin");
        File fWrite = new File("5228.txt");
        try{  Writer out = new FileWriter(fWrite);
            BufferedWriter bufferWrite = new BufferedWriter(out);
            Reader in = new FileReader(fRead);
            BufferedReader bufferRead =new BufferedReader(in);
            String str = null;
            for(int i=1;i<11;i++) {
                str=bufferRead.readLine();
                str=i+" "+str;
                bufferWrite.write(str);
                bufferWrite.newLine();
            }
            bufferWrite.close();
            out.close();
        }
        catch(IOException e) {
            System.out.println(e.toString());
        }
    }
}