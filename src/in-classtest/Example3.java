import java.sql.*;
public class Example3 {
    public static void main(String [] args) {
        Connection con=null;
        Statement sql;
        ResultSet rs;
        float min=100.0f,max=0.0f;
        int i=0;
        String minname=new String("");
        String maxname=new String("");
        String sqlStr =
                "select * from country order by LifeExpectancy";
        try{  Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){}
        String uri = "jdbc:mysql://localhost:3306/world?useSSL=true";
        String user ="root";
        String password ="";
        try{
            con = DriverManager.getConnection(uri,user,password);
        }
        catch(SQLException e){ }
        try {
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while(rs.next()) {
                String Name=rs.getString(2);
                Float LifeExpectancy=rs.getFloat(8);
                if(LifeExpectancy>max) {
                    max =LifeExpectancy;
                    maxname=Name;
                }
                else if((LifeExpectancy<min)&&(LifeExpectancy!=0.0)){
                    {
                        min = LifeExpectancy;
                        minname = Name;
                    }
                }

            }
            con.close();
            System.out.printf("平均寿命最长的国家:"+maxname+"\n平均寿命为:"+max+"\n");
            System.out.printf("平均寿命最短的国家:"+minname+"\n平均寿命为:"+min+"\n");
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}

